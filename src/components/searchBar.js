import React, {Component} from 'react';

class SearchBar extends Component {
    // state = {term: 'New York'}
    constructor(props) {
        super(props)
        this.state = {city: ''} // default to show on searchbar
    }

    onProcessSubmit(evt) {
        evt.preventDefault() // no need to refresh; changes default action
        // console.log(this.state.city) // for testing
        this.props.recvCity(this.state.city) // this=SearchBar; this.props.recvTerm=recvNewTerm
    }

    render() {
        return (
            <div>
                <form className="ui form" onSubmit={evt => this.onProcessSubmit(evt)}>
                    <div className="filled">
                        <div className="ui icon input loading">
                            <input
                                onChange={evt => this.setState({city: evt.target.value.trimLeft()})} // update the term
                                value={this.state.city} // read the term; let value = the term
                                type="text" placeholder="Search..."/>
                            <i className="search icon"></i>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default SearchBar;