import React, {Component} from 'react';
import './weatherComponent.css'
import axios from "axios";
import Loader from "./loader"

const weatherUrl = 'http://api.weatherstack.com/current'
const weatherAKey = '353457be150e97419fa9acbcf147a775'
const picUrl = 'https://api.unsplash.com/photos'
const picAKey = 'IYVnwjHFZo-6sKC1ln9LYf2pYC9IM-ZhkafLdSg1LzI'

class WeatherComponent extends Component {
    state = {
        loading: true,
        city: '', // correct
        images: '', // or new Array()
        weather: {}
    }

    constructor(props) {
        super(props)
    }

    updateCityQuery() { // secret key: OYqEQRJvQgt0_b8qXxRkuGXTx8g_eD5VlDo8sCDw_Fw
        let city = this.props.passCity // this.state.city doesn't update in time

        // get weather from server
        axios.get(`${weatherUrl}?access_key=${weatherAKey}&query=${city}`)
            .then(res => {
                console.log(res)
                this.setState({
                    loading: false,
                    weather: res.data
                })
            })
            .catch(err => {
                console.log(err)
                this.setState({loading: false})
            })

        // get pics from server
        axios.get(`${picUrl}`, {
            params: {
                query: city
            },
            headers: {
                Authorization: `Client-ID ${picAKey}`
            }
        })
            .then(res => {
                console.log(res)
                this.setState({
                    images: res.data // update within the component
                })
                this.props.recvImg(this.state.images) // then send the pics onto the prop to show
            })
            .catch(err => {
                console.log(err)
            })
    }

    componentDidMount() {
        this.updateCityQuery()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.passCity !== this.props.passCity) {
            this.setState({city: this.props.passCity})
            this.updateCityQuery()
        }
    }

    componentWillUnmount() {
    }

    render() {
        if (this.state.loading) {
            return (
                <Loader message="努力加载中..."/>
            )
        }
        return ( //this.state.images doesn't work
            <div className="weather"
                 style={{backgroundImage: `url(${this.props.passImg})`}}>
                <div className="weatherRow">
                    <div className="weatherCol">{this.state.weather?.location?.name}, {this.state.weather.location.region}, {this.state.weather.location.country}</div>
                </div>
                <div className="weatherRow" style={{marginLeft: "50px", marginRight: "50px"}}>
                    <div className="weatherCol">
                        <img src={this.state.weather?.current?.weather_icons[0]} alt=""/>
                        {this.state.weather?.current?.weather_descriptions[0]}
                    </div>
                    <div className="weatherCol" style={{fontSize: "32px", margin: "auto"}}>
                        {this.state.weather?.current?.temperature}°c
                    </div>
                    <div className="weatherCol" style={{alignItems: "center"}}>
                        Wind: {this.state.weather?.current?.wind_speed} kmph
                        <br/>
                        Precip: {this.state.weather?.current?.precip} mm
                        <br/>
                        Pressure: {this.state.weather?.current?.pressure} mb
                    </div>
                </div>
                <div className="weatherRow">
                    <div className="weatherCol" >
                        <div>FRI</div>
                        <div>2</div>
                        <div>3</div>
                    </div>
                    <div className="weatherCol">
                        <div>FRI</div>
                        <div>2</div>
                        <div>3</div>
                    </div>
                    <div className="weatherCol">
                        <div>FRI</div>
                        <div>2</div>
                        <div>3</div>
                    </div>
                    <div className="weatherCol">
                        <div>FRI</div>
                        <div>2</div>
                        <div>3</div>
                    </div>
                    <div className="weatherCol">
                        <div>FRI</div>
                        <div>2</div>
                        <div>3</div>
                    </div>
                </div>
            </div>)
    }
}

export default WeatherComponent;
