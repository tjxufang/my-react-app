import React, {Component} from 'react';
import './imageList.css'

class ImageList extends Component {
    render() {
        let imgToShow = this.props.passImg.map(obj => {
            return (
                <img
                    key={obj.id}
                    src={obj.urls.regular}
                    className="single"
                    onClick={evt => this.props.updateImgClk(evt.target)}
                ></img>
            )
        })

        return (
            <div className="imageList">{imgToShow}</div>
        )
    }
}

export default ImageList;