import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import WeatherComponent from "./components/weatherComponent";
import SearchBar from "./components/searchBar"
import ImageList from "./components/imageList"

class App extends Component {
    state = {
        city: 'nyc', // default to get data from server
        images:[],
        bgImg: null,
    }

    recvNewCity(city) {
        this.setState({city}) // = {term: term}; but doesn't work
    }

    recvNewImg(images) {
        // console.log("before setState for images", images)
        this.setState({images})
        // console.log("after setState for images", images)
    }

    render() {
        //pass=in recv=out
        return (
            <div className="ui minimal comments" style={{margin: '20px 20px'}}>
                <h3 className="ui dividing header">Weather</h3>
                <SearchBar
                    recvCity={city => this.recvNewCity(city)}
                />
                <br/>
                <WeatherComponent
                    passCity={this.state.city} // IN: city name from searchbar (now manually input)
                    passImg={this.state.bgImg} // IN: for background change
                    recvImg={img => this.recvNewImg(img)} // OUT: got pics from server
                />
                <ImageList
                    passImg={this.state.images} // IN: from weatherComp
                    updateImgClk={img => this.setState({bgImg: img.src})} // OUT: from onClick
                />
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.querySelector('#root'))